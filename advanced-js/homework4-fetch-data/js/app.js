const requestURL = 'https://swapi.dev/api/films/';
const moviesList = document.createElement('ul');
document.body.prepend(moviesList);
moviesList.classList.add('movies__list');

const sendRequest = url => fetch(url).then(response => response.json());

const renderMovie = movieData => {
    const {episode_id, title, opening_crawl} = movieData;
    const movieItem = `
        <ul class="movie__item">
            <li>Episode number - <strong>${episode_id}</strong></li>
            <li>Movie title - <strong>${title}</strong></li>
            <li><ins>Short description of the film</ins>: <strong>${opening_crawl}</strong></li>
        </ul>
    `;
    moviesList.insertAdjacentHTML('afterbegin', movieItem);
    const movie = document.querySelector('.movie__item');
    movie.insertAdjacentHTML('beforeend', `<ul class="movie__item-actors"><h3>Actors:</h3></ul>`);
};

const renderActors = link => {
    const movieItemActors = document.querySelector('.movie__item-actors');
    sendRequest(link)
        .then(actorData => {
            movieItemActors.insertAdjacentHTML('beforeend', `<li>${actorData.name}</li>`);     
        })
    };

sendRequest(requestURL)
    .then(data => {
            data.results.forEach(movieData => {
                renderMovie(movieData);     
                movieData.characters.forEach(link => {
                    renderActors(link)
                }) 
            })
    });















































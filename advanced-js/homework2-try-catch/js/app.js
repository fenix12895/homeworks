const books = [
    { 
      author: "Скотт Бэккер",
      name: "Тьма, что приходит прежде",
      price: 70 
    }, 
    {
     author: "Скотт Бэккер",
     name: "Воин-пророк",
    }, 
    { 
      name: "Тысячекратная мысль",
      price: 70
    }, 
    { 
      author: "Скотт Бэккер",
      name: "Нечестивый Консульт",
      price: 70
    }, 
    {
     author: "Дарья Донцова",
     name: "Детектив на диете",
     price: 40
    },
    {
     author: "Дарья Донцова",
     name: "Дед Снегур и Морозочка",
    }
  ];
  
const elemenstList = document.createElement('ul');
const root = document.querySelector('#root');
root.append(elemenstList);

books.forEach((element) => {
    try {
        if (element.name == undefined) {
            throw new Error('Помилка, в об\'єкті немає ім\'я');  
        } else if (element.price == undefined) {
            throw new Error('Помилка, в об\'єкті немає ціни');
        } else if (element.author == undefined) {
            throw new Error('Помилка, в об\'єкті немає автора');
        }
        elemenstList.insertAdjacentHTML('beforeend', `<ul><li>${element.author}</li><li>${element.name}</li><li>${element.price}</li></ul>`);
        
    } catch(err) {
        console.error(err);
    }
})



class Employee {
    constructor(name = 'John', age = 20, salary = 2000) {
        this.name = name;
        this.age = age;
        this.salary = salary;
    }
    get getName() {
        return this.name;
    }
    set setName(name) {
        this.name = name;
    }
    get getAge() {
        return this.age;
    }
    set setAge(age) {
        this.age = age;
    }
    get getSalary() {
        return this.salary;
    }
    set setSalary(salary) {
        this.salary = salary;
    }
}

class Programmer extends Employee {
    constructor(name, age, salary, lang) {
        super(name, age, salary);
        this.lang = lang;
    }
    get getSalary() {
        return this.salary * 3;
    }
}

const john = new Programmer('John', 20, 2000, ['ua', 'ru', 'en']);
console.log(john);
const alex = new Programmer('Alex', 22, 2500, ['ua', 'ru', 'en', 'de']);
console.log(alex);

const card = `
    <div class="card">
        <button class="find">Find by IP</button>
        <p class="text">IP address information:</p>
        <p class="output"></p>
    </div>
`;
document.body.insertAdjacentHTML('afterbegin', card);
const button = document.querySelector('.find');
const output = document.querySelector('.output');

async function getIpAddressFn() {
    const getIpAddressResponse  = await fetch('https://api.ipify.org/?format=json');
    const getIpAddressData = await getIpAddressResponse.json();
    return getIpAddressData;
}

async function getIpInfoFn() {
    const ipAddress  = await getIpAddressFn();
    const getIpInfoResponse = await fetch(`http://ip-api.com/json/${ipAddress.ip}?fields=continent,region,country,city,district`);
    const getIpInfoData = await getIpInfoResponse.json();
    return getIpInfoData;
}

function addIpInfoToOutputFn(data) {
    const {continent, region, country, city, district} = data;
    const info = `
        <p class="${continent ? 'output-text-true' : 'output-text-false'}">Continent - ${continent}</p>
        <p class="${region ? 'output-text-true' : 'output-text-false'}">Region - ${region}</p>
        <p class="${country ? 'output-text-true' : 'output-text-false'}">Country - ${country}</p>
        <p class="${city ? 'output-text-true' : 'output-text-false'}">City - ${city}</p>
        <p class="${district ? 'output-text-true' : 'output-text-false'}">District - ${district ? district : 'unknown district'}</p>
    `;
    output.insertAdjacentHTML('beforeend', info);
}

async function renderIpInfo() {
    output.innerHTML = '';
    const ipInfo = await getIpInfoFn()
    addIpInfoToOutputFn(ipInfo); 
}

button.addEventListener('click', renderIpInfo);




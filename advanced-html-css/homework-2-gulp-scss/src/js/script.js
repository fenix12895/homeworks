// Burger menu

const burgerMenuBtn = document.querySelector('.header__menu-burger');
const burgerMenuBtnSpan1 = document.querySelector('.header__menu-burger span:nth-child(1)');
const burgerMenuBtnSpan2 = document.querySelector('.header__menu-burger span:nth-child(2)');
const burgerMenuBtnSpan3 = document.querySelector('.header__menu-burger span:nth-child(3)');
const headerMenuList = document.querySelector('.header__menu-list');

burgerMenuBtn.addEventListener('click', function() {
    headerMenuList.classList.toggle('header__menu-list--active');
    burgerMenuBtnSpan1.classList.toggle('first');
    burgerMenuBtnSpan2.classList.toggle('middle');
    burgerMenuBtnSpan3.classList.toggle('last');
})
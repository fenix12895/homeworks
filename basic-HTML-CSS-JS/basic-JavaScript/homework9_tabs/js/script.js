let buttonsTab = document.querySelector('.tabs');
const BACKGROUND_COLOR_TAB_1= '#937341';
const BACKGROUND_COLOR_TAB_2 = '#23201D';
const COLOR_TEXT_TAB_1 = '#23201D';
const COLOR_TEXT_TAB_2 = '#937341';

function changeTabsOnClick(event) {
    if (event.target.className == 'tabs-title') {
        let dataTab = event.target.getAttribute('data-tab');
        let tabContentItem = document.querySelectorAll('.tab-content-item');
        let tabsTitle = document.querySelectorAll('.tabs-title');
        for (let i = 0; i < tabsTitle.length; i++ ) {
            tabsTitle[i].classList.remove('tabs-title-active');
        }
        for (let i = 0; i < tabContentItem.length; i++) {
            if (dataTab == i) {
                tabContentItem[i].style.display = 'block';
                for (let i = 0; i < tabsTitle.length; i++) {
                    if (dataTab == i) {
                        tabsTitle[i].style.backgroundColor = BACKGROUND_COLOR_TAB_1;
                        tabsTitle[i].style.color = COLOR_TEXT_TAB_1;
                        tabsTitle[i].style.fontWeight = '700';
                    } else {
                        tabsTitle[i].style.backgroundColor = BACKGROUND_COLOR_TAB_2 ;
                        tabsTitle[i].style.color = COLOR_TEXT_TAB_2;
                        tabsTitle[i].style.fontWeight = '400';
                    }
                }
            } else {
                tabContentItem[i].style.display = 'none'; 
            }
        }
    }
}

buttonsTab.addEventListener('click', changeTabsOnClick);
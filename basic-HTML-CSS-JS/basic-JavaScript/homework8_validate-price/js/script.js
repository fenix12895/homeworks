let inputEl = document.querySelector('#input');
let formEl = document.querySelector('#form');
let spanElTop = document.createElement('span');
let spanElBottom = document.createElement('span');
let btnDelete = document.createElement('button');

inputEl.addEventListener('focus', () => inputEl.classList.add('focused-green'), true);
inputEl.addEventListener('blur', () => inputEl.classList.remove('focused-green'), true);

function addPriceOnBlur() {
    if (!inputEl.value) return;
    if (inputEl.value > 0) {
        inputEl.classList.remove('focused-red');
        inputEl.classList.add('focused-green');
        formEl.before(spanElTop);
        spanElTop.after(btnDelete);
        btnDelete.innerHTML = 'x';
        spanElTop.innerHTML = `Текущая цена: ${inputEl.value}`;   
        inputEl.style.color = 'green'; 
        inputEl.style.fontWeight = '700'; 
        btnDelete.style.display = 'inline-block';
    } else {
        inputEl.classList.remove('focused-green');
        inputEl.classList.add('focused-red');
        formEl.after(spanElTop);
        btnDelete.style.display = 'none';
        spanElTop.innerHTML = 'Please enter correct price'; 
    }  
}

function removeSpanAndBtnDelete () {
    spanElTop.remove();
    btnDelete.remove();
    inputEl.value = '';
}
btnDelete.addEventListener('click', removeSpanAndBtnDelete);
inputEl.addEventListener('blur', addPriceOnBlur);


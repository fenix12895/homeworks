let imagesToShowEL = document.querySelectorAll('.image-to-show');
let imagesWrapperEl = document.querySelector('.images');
let stopSliderEl = document.querySelector('.control-button-stop');
let startSliderEl = document.querySelector('.control-button-start');
let controlButtonsEl= document.querySelector('.control-buttons');
let outputEl = document.querySelector('.output-block');

let current = 1;
let interval;
let interval2;
let counter = 3;


function changeClassInImagesFn() {
    for (let i = 0; i < imagesToShowEL.length; i++) {
        imagesToShowEL[i].classList.add('image-hide');
    }
    imagesToShowEL[current].classList.remove('image-hide');

    if (current === imagesToShowEL.length - 1) {
        current = 0;
    } else {
        current += 1
    }
};

function sliderFn() {
    interval = setInterval(changeClassInImagesFn, 3000);
    startSliderEl.disabled = true;
};
sliderFn();
function counterFn () {
    counter--;
    outputEl.innerHTML = `До показу наступного слайду залишилось <span>${counter}<span>`
    if (counter === 1) {
        counter = 4;
    }
}
function startCounter() {
    interval2 = setInterval(counterFn, 1000);
}
startCounter() 
setTimeout(() => {
    controlButtonsEl.classList.add('show-control-buttons');
}, 2000);

stopSliderEl.addEventListener('click', () => {
    startSliderEl.disabled = false;
    clearInterval(interval);
    clearInterval(interval2)
});
startSliderEl.addEventListener('click', () => {
    sliderFn();
    startCounter();
});


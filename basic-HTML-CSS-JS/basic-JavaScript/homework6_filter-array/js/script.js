// Рішення за допомогою методу filter
function filterBy(arr, dataType) {
    newArr = arr.filter((element) => {
        return typeof(element) !== typeof(dataType)
    })
    return newArr
}
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));


// Рішення за допомогою циклу for of
function filterBy(arr, dataType) {
    let newArr = [];
    for (let element of arr) {
        if (typeof(element) !== typeof(dataType)) {
            newArr.push(element)  
        }
    }
    return newArr
}
console.log(filterBy(['hello', 'world', 23, '23', null], 'string'));





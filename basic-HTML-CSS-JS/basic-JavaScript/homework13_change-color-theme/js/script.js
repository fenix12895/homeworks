const bodyEl = document.querySelector('.body');
const postsEl = document.querySelector('.posts');
const headerThemeBtnEl = document.querySelector('.header-theme-btn');
const HEADER_THEME_BTN_COLOR_WHITE = '#fff';
const HEADER_THEME_BTN_COLOR_BLACK = '#000';

function changeThemeColor() {
    bodyEl.classList.toggle('body-color-theme');
    postsEl.classList.toggle('posts-color-theme');
    if (bodyEl.classList.contains('body-color-theme')) {
        headerThemeBtnEl.textContent = 'White Theme';
        headerThemeBtnEl.style.color = HEADER_THEME_BTN_COLOR_WHITE;
        localStorage.setItem('color', 'color');
    } else {
        headerThemeBtnEl.textContent = 'Black Theme';
        headerThemeBtnEl.style.color = HEADER_THEME_BTN_COLOR_BLACK;
        localStorage.removeItem('color');
    } 
}

headerThemeBtnEl.addEventListener('click', changeThemeColor);

if (localStorage.getItem('color')) {
    bodyEl.classList.add('body-color-theme');
    postsEl.classList.add('posts-color-theme'); 
    headerThemeBtnEl.textContent = 'White Theme';
    headerThemeBtnEl.style.color = HEADER_THEME_BTN_COLOR_WHITE;
} else {
    bodyEl.classList.remove('body-color-theme');
    postsEl.classList.remove('posts-color-theme'); 
    headerThemeBtnEl.textContent = 'Black Theme';
    headerThemeBtnEl.style.color = HEADER_THEME_BTN_COLOR_BLACK;  
}


// Створюємо змінну num та записуємо в ню число, яке введе користувач.
let num = prompt("Введіть число", 1);
// Перевіряємо введене число користувачем на корректність.
while (isNaN(num) || num == "") {
  num = prompt("Введіть число", 1);
}
// Створюємо ф-цію, яка рахує факторіал числа, яке вводить користувач.
function calculateFactorial(n) {
  if (n >= 1) {
    return n * calculateFactorial(n - 1);
  } else {
    return 1;
  }
}
// Виводимо на екран результат дії ф-ції.
alert(calculateFactorial(num));

const array = ["hello", "world", "Kiev", "Kharkiv", "Odessa", "Lviv"];
const array2 = ["hello", "world", "Kiev", ["Borispol", "Irpin"], "Kharkiv", "Odessa", "Lviv"];

function addOnPageArrayElements(arr, parent = document.body) {
    const elementsList = document.createElement('ul');
    parent.append(elementsList)
    arr.map((item) => {
        elementsList.insertAdjacentHTML('beforeend', `<li>${item}</li>`);
    });  
};
addOnPageArrayElements(array);

//------------------------------------------------------

// Advanced 
function addOnPageArrayElements(arr, parent = document.body) {
    const elementsList = document.createElement('ul');
    const elementsSubList = document.createElement('ul');
    parent.append(elementsList);
    elementsList.append(elementsSubList);
    arr.map((item) => {
        if (typeof item === 'string') {
            const elemLi = document.createElement('li');
            elementsList.append(elemLi);
            elemLi.innerHTML = item;
        } else {
            item.map((subItem) => {
                const elemsSubLi = document.createElement('li'); 
                elementsSubList.append(elemsSubLi);
                elemsSubLi.innerHTML = subItem;  
                elementsList.append(elementsSubList);
            }); 
        };
    });  
    function removeContent() {
       elementsList.remove(); 
    };
    setTimeout(removeContent, 4000);
};
addOnPageArrayElements(array2);

const elementNumber = document.createElement('div');
document.body.append(elementNumber);
elementNumber.style.fontSize = '50px';
elementNumber.style.fontWeight = 700;
elementNumber.style.color = 'red';
elementNumber.style.marginLeft = '20px';
function counter() {
    n = 4;
    let timer = setInterval(() => {
        n -= 1;
        elementNumber.innerHTML = n;
        if (n === 0) {
           clearInterval(timer);
           elementNumber.innerHTML = '';
        };
    }, 1000);
};
counter();



















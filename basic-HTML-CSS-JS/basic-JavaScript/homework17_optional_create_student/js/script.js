// Створюємо пустий об'єкт student
let student = {};
// Запитуємо у користувача ім'я та прізвище студента
student.firstName = prompt('Введіть Ваше ім\'я:');
student.lastName = prompt('Введіть Ваше прізвище:');
// Добавляємо в об'єкт student пустий підоб'єкт tabel
student.tabel = {};
// Створюємо змінні badMark, ratingCounter та averageBall
let badMark = 0;
let ratingCounter= 0;
let averageBall;
/*За допомогою цилку while добавляємо в підоб'єкт tabel предмети та оцінки. 
Якщо користувач нажимає на кнопку cancel цикл переривається.*/
while (true) {
	let subject = prompt('Введіть назву предмету:');
	if (subject === null) {
		break;
	};	
	let grade = +prompt('Введіть оцінку');
	student.tabel[subject] = grade;
};
/*За допомогою циклу for перевіряємо кількість оцінок, які менші за 4 та рахуємо загальну кількість оцінок*/
for (let rating in student.tabel) {
	if (student.tabel[rating] < 4) {
		badMark++;
	}
	ratingCounter++;
};
// Якщо поганих оцінок немає, то студента переводять на наступний курс.
if (badMark === 0) {
	console.log(`Студент ${student.firstName} ${student.lastName} переведений на наступний курс.`);
} else {
	console.log(`Студент ${student.firstName} ${student.lastName} повинен пройти курс повторно.`);
};
// За допомогою методу reduce знаходимо сумарний бал по всім предметам.
totalBall = Object.values(student.tabel).reduce((sum, current) => {
	return sum + current;
}, 0);
// Знаходимо середній бал
averageBall = totalBall / ratingCounter;
console.log(`Середній бал студента ${student.firstName} ${student.lastName} - ${averageBall}.`);
// Якщо середній бал більше 7, то студенту назначена стипендія.
if (averageBall > 7) {
	console.log(`Студенту ${student.firstName} ${student.lastName} призначена стипендія.`);
};




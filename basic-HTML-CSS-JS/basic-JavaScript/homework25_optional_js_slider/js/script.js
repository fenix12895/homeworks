    const itemWidth = '256';
    const amountImgages = 6;
    const track = document.querySelector('.slider-track');
    const item = document.querySelectorAll('.slider-item');
    const btnPrev = document.querySelector('.btn-prev');
    const btnNext = document.querySelector('.btn-next');
    let position = 0;
    let widthAllImages = itemWidth * amountImgages;

    btnNext.addEventListener('click', () => {
        if (position <= -(widthAllImages - itemWidth)) {
            position = 0;
             track.style.transform = `translateX(${position}px)`;
        } else {
            position -= +itemWidth;
            track.style.transform = `translateX(${position}px)`;
        }
    })
    btnPrev.addEventListener('click', () => {
        if (position >= 0) {
            position = -(widthAllImages - itemWidth);
            track.style.transform = `translateX(${position}px)`;
        } else {
            position += +itemWidth;
            track.style.transform = `translateX(${position}px)`;
        }
    })

    
  

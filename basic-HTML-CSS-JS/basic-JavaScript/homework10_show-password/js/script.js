const inputPasswordEl = document.querySelector('.input-password');
const confirmPasswordEl = document.querySelector('.confirm-password');
const confrimButton = document.querySelector('.btn');
const inputIconShowEl = document.querySelector('#input-icon-show');
const inputIconHideEl = document.querySelector('#input-icon-hide');
const confirmIconShowEl = document.querySelector('#confirm-icon-show');
const confirmIconHideEl = document.querySelector('#confirm-icon-hide');
const divEl = document.createElement('div');

function hideShowIcon(item1, item2) {
    item1.style.display = 'block';
    item2.style.display = 'none';
}
hideShowIcon(inputIconShowEl, inputIconHideEl);
hideShowIcon(confirmIconShowEl, confirmIconHideEl);

inputIconShowEl.addEventListener('click', () => {
    hideShowIcon(inputIconHideEl, inputIconShowEl);
    inputPasswordEl.setAttribute('type', 'text');
})
inputIconHideEl.addEventListener('click', () => {
    hideShowIcon(inputIconShowEl, inputIconHideEl);
    inputPasswordEl.setAttribute('type', 'password');
});
confirmIconShowEl.addEventListener('click', () => {
    hideShowIcon(confirmIconHideEl, confirmIconShowEl);
    confirmPasswordEl.setAttribute('type', 'text');
})
confirmIconHideEl.addEventListener('click', () => {
    hideShowIcon(confirmIconShowEl, confirmIconHideEl);
    confirmPasswordEl.setAttribute('type', 'password');
});

confrimButton.addEventListener('click', event => {
    event.preventDefault();
    if (inputPasswordEl.value === confirmPasswordEl.value) {
     alert('You are welcome');
     divEl.innerHTML = '';
    } else {
       divEl.innerHTML = 'Нужно ввести одинаковые значения';
       divEl.style.color = 'red'
       confirmPasswordEl.after(divEl);
    }
 });







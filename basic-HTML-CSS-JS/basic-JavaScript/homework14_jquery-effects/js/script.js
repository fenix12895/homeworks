// Плавна прокрутка до якорів
$(function () {
    $(('a[href*="#"]')).on("click", function(event){
    const anchor = $(this);
    $('html, body').stop().animate({
    scrollTop: $(anchor.attr('href')).offset().top
    }, 1000);
    event.preventDefault();
    return false;
    });
});

// Кнопка наверх
$(document).ready(function() { 
    const buttonToTop = $('.header-btn-to-top');	
    $(window).scroll (function () {
      if ($(this).scrollTop () > 500) {
        buttonToTop.fadeIn();
      } else {
        buttonToTop.fadeOut();
      }
  });	 
  buttonToTop.on('click', function(){
    $('body, html').animate({
    scrollTop: 0
    }, 800);
    return false;
  });		 
});

// Показати/приховати зображення в розділі "MOST POPULAR POSTS"
$(function() { 
  const postBtnToggle = $('.post-btn-toggle');
  postBtnToggle.on('click', function() {
    $('.posts-images').toggle('.posts-images-hide')
  })
  postBtnToggle.click(function(){
    if (!$(this).data('status')) {
      $(this).html('SHOW POSTS');
      $(this).data('status', true);
    }
    else {
      $(this).html('HIDE POSTS');
      $(this).data('status', false);
    }
  });
});


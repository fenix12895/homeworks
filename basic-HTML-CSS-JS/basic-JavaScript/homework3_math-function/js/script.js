// Створюємо змінні в які ініціалізуємо дані, які введе корситувач.
let number1 = +prompt("Введіть перше число:", 1);
// Перевірка на корректність введеного користувачем числа.
checkNumbers(number1);
let operation = prompt("Введіть математичну операцію: +, -, *, /");
let number2 = +prompt("Введіть друге число:", 1);
// Перевірка на корректність введеного користувачем числа.
checkNumbers(number2);
let result;
// Створюємо функцію, яка перевіряє правильність введених даних.
function checkNumbers(num) {
  while (isNaN(num) || num == "") {
    num = +prompt("Введіть корректно число:", 1);
  }
}
// Створюємо функцію, яка робить математичні операції над числами, які введе користувач за допомогою prompt.
function doOperationsOfNumbers(n1, n2, op) {
  if (op == "+") {
    result = n1 + n2;
  } else if (op == "/") {
    result = n1 / n2;
  } else if (op == "*") {
    result = n1 * n2;
  } else if (op == "-") {
    result = n1 - n2;
  }
  return result;
}
// Визиваємо функцію, яка робить оперції над числами та передаємо в ню аргументи.
doOperationsOfNumbers(number1, number2, operation);
// Виводимо в консоль результат ф-ції.
console.log(`Результат дії над числами: ${result}`);

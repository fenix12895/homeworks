import React, {Component} from 'react'
import  styles from './ProductItem.module.scss'
import PropTypes from 'prop-types'
class ProductItem extends Component {
    state = {
        isFavorite: false
    }   
    setFavorite = (id) => {
        this.setState({isFavorite: !this.state.isFavorite})
        if (!localStorage.getItem('favorite')) {
            localStorage.setItem('favorite', id)
        } else {
            const favoriteArr = localStorage.getItem('favorite').split(',')
            if (!favoriteArr.includes(id)) {
               favoriteArr.push(id)
               localStorage.setItem('favorite', favoriteArr)
            } 
            else {
                const newFavorite = favoriteArr.filter(item => item !== id)
                localStorage.setItem('favorite', newFavorite)
            }
        }
    }
    componentDidMount() {
        if (localStorage.getItem('favorite')) {
           const arr = localStorage.getItem('favorite').split(',')
           if (arr.includes(this.props.product.id)) {
            this.setState({isFavorite: !this.state.isFavorite})
           }
        }
    }
    render() {
        const {img, name, id, code, price, color} = this.props.product

        let styleColor = ''
        if (color === 'Red') {
            styleColor = styles.Red
        } else if (color === 'Blue') {
            styleColor = styles.Blue
        } else {
            styleColor = styles.Black
        }

        return (
            <li className={styles.ProductItem}>
                <div className={styles.ProductItemImg}>
                    <img 
                        src={"db/" + img} alt={name}
                    />
                </div>
                <div className={styles.Content}>
                    <div className={styles.ProductItemName}>
                        {name}
                    </div>
                    <div className={styles.ProductItemColor}>
                        Color: <span className={styleColor}>{color}</span>
                    </div>
                    <div className={styles.Star} onClick={() => this.setFavorite(id)}>  
                        {this.state.isFavorite ? <i className="fas fa-star" style={{cursor: 'pointer', color: 'purple'}}></i>  :  <i className="far fa-star" style={{cursor: 'pointer', color: 'purple'}}></i>}
                    </div>
                    <div className={styles.ProductItemCode}>
                        Code: {code}
                    </div>
                    <div className={styles.ProductItemBottom}>
                        <div className={styles.ProductItemPrice}>
                            Price: {price}$
                        </div>
                        <button 
                            onClick={() => this.props.addToCart(id)}
                            className={styles.Button}>
                            Add to cart
                            &nbsp;
                                <i className="fas fa-shopping-cart"></i>
                        </button>
                    </div>
                </div>
            </li>
        )
    }
}

ProductItem.propTypes = {
    img: PropTypes.string,  
    name: PropTypes.string,  
    id: PropTypes.string,  
    code: PropTypes.string,  
    price: PropTypes.string

}

export default ProductItem
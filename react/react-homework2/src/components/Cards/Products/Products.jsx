import React, {Component} from 'react'
import styles from './Products.module.scss'
import ProductItem from './ProductItem/ProductItem'
import PropTypes from 'prop-types'

class Products extends Component {
    render() {   
        return (
            <ul className={styles.Products}>
                {this.props.products.map((product) => {
                    return (
                        <ProductItem
                            addToCart={this.props.addToCart}
                            key={product.id}
                            product={product}
                        />
                    )
                })}
            </ul>
        )
    }
}

Products.propTypes = {
    addToCart: PropTypes.func  
}

export default Products
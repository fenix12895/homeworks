import React, {Component} from 'react'
import style from './App.module.scss'
import Button from './components/UI/Button/Button'
import Modal from './components/ModalComponents/Modal/Modal'
import Products from './components/Cards/Products/Products'
import getProducts from './api/getProducts'


class App extends Component {

  state = {
    products: [],
    isOpenModal: false,
    isLoadingProducts: true,
    productInCart: []
  }

  componentDidMount() {
    getProducts()
      .then(products => {
        this.setState({
          products: products,
          isLoadingProducts: false,
          productInCart: [localStorage.getItem('cart')]
        })
      })
  }

  openModal = (id) => {
    this.setState({
      isOpenModal: true, 
      productInCart: [this.state.productInCart, id]
    });
  }
    
  closeModal = () => {
      this.setState({isOpenModal: false})
  }

  handleAddToCart = () => {
    localStorage.setItem('cart', this.state.productInCart)
    this.closeModal()
  }

  render() {
    return (
      <div className={style.App}>
        <h1 style={{color: '#fff', textAlign: 'center'}}>Product cards</h1>
        
        {/* Modal window */}
        {this.state.isOpenModal &&
            <Modal 
              onClick={this.closeModal}
              header="Confirm adding to cart this product"
              closeButton={this.closeModal} 
              text="Lorem ipsum dolor sit, amet consectetur adipisicing elit."
              actions={
                <>
                  <Button text="Add to cart" onClick={this.handleAddToCart}/>
                  <Button text="Cancel" onClick={this.closeModal}/>
                </>
              }
            />
        }
        {/* =============================== */}

        {/* Products */}
         {!this.state.isLoadingProducts && 
            <Products 
              products={this.state.products}
              addToCart={this.openModal}
            />
          }
        {/* =============================== */}
      </div>
    )
  }
}

export default App;






